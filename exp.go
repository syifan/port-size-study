package main

import (
	"log"
	"os"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/gcn3/benchmarks/fir"
)

func main() {
	_, gpu, gpuDriver, _ := BuildR9NanoPlatform()

	benchmark := fir.NewBenchmark(gpuDriver)
	benchmark.Length = 65536

	traceOutput, err := os.Create("conn_trace.trace")
	if err != nil {
		log.Panic(err)
	}
	connTracer := akita.NewConnectionTracer(traceOutput)
	gpu.InternalConnection.AcceptHook(connTracer)

	benchmark.Run()
}
